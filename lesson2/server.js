const server = require('http').createServer();
const io = require('socket.io')(server);
io.on('connection', client => {
  console.log('someone connected');
  client.on('send-message', message => {
    console.log(`someone said ${message}`);
  });
  client.on('disconnect', () => { 
    console.log('someone disconnected');
  });
});
server.listen(3000);