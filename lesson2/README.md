# Lesson 2
In this lesson, we will start sending messages and handle corresponding events on both client and server-side.
## Step 1: Prepare index.html
First, we need a container that holds the messages and then form to send a message from it:
```html
<body>
  <div id="message-container"></div>
  <form id="send-container">
    <input type="text" id="message-input">
    <button type="submit" id="send-button">Send</button>
  </form>
</body>
```
Also lets add some basic styling:
```css
body {
  padding: 0;
  margin: 0;
  display: flex;
  justify-content: center;
}

#message-container {
  width: 80%;
  max-width: 1200px;
}

#send-container {
  position: fixed;
  padding-bottom: 30px;
  bottom: 0;
  background-color: white;
  max-width: 1200px;
  width: 80%;
  display: flex;
}

#message-input {
  flex-grow: 1;
}
```
Bad thing about `HTML` forms is that by default when you submit them they will send the user to the `action` page with the data of the form, but we don't want it to happen, we want to stay in the same page, so in the `script.js` we will do a `preventDefault()` that prevents the default action of the form from happening.
```javascript
const messageForm = document.getElementById('send-container')
messageForm.addEventListener('submit', event => {
  event.preventDefault();
});
```
Now if you click the Send button in the form nothing happens.
## Step 2: Send a message to server
First for simlicity sake, lets separate event listener function in a separate function call `onSubmit()`
```javascript
messageForm.addEventListener('submit', onSubmit());

function onSubmit(event){
  event.preventDefault();
}
```
To send a message to server we need to use the same `EventListener` that we used on the form to stop form from submitting. In our `script.js` we need will define our message box:
```javascript
const messageInput = document.getElementById('message-input')
```
Now that we have our message input object in a variable, we can use all the `methods`, and `properties` that come with it. One of these `properties` is `value` which will give us the value or the text of a field. Then we will use a method of our `socket` object called `emit` which will send an event to the server along with its value.
```javascript
function onSubmit(event){
  event.preventDefault();
  const message = messageInput.value;
  socket.emit('send-message', message);
}
```
## Step 3: Get the message on server
As we mentioned before in the server script on the first lesson, the `socket.IO` method `on` connection can handle custom events, so we will add our newly created event `send-message` to this method:
```javascript
io.on('connection', client => {
  console.log('someone connected');
  client.on('send-message', message => {
    console.log(`someone said ${message}`);
  });
  client.on('disconnect', () => { 
    console.log('someone disconnected');
  });
});
```
So now If you submit your message, in the terminal you will see your sent message to the server.
## Final step: cleaning the house
Everything works but nothing seems to happen in our screen as a client. let's change that. first we will clear our input field when the message is sent. So in the `onSubmit()` function we will empty the field by assigning an empty string to the `value` property:
```javascript
function onSubmit(event){
  event.preventDefault();
  const message = messageInput.value;
  socket.emit('send-message', message);
  messageInput.value = '';
}
```
Next we will add a function that adds a child to our message container in the `index.html` file called `addMessage()` but also for that we need to define our message container object:
```javascript
const messageContainer = document.getElementById('message-container');

function addMessage(message) {
  const messageElement = document.createElement('div')
  messageElement.innerText = message
  messageContainer.append(messageElement)
}
```
As you can see, this function will add a `div` with the message as the inside text. So lets call it in our `onSubmit()` function:
```javascript
function onSubmit(event){
  event.preventDefault();
  const message = messageInput.value;
  socket.emit('send-message', message);
  messageInput.value = '';
  addMessage(`You: ${message}`);
}
```
## Well done!
Good, now we are successfully sending message to our server and have a little preview of it on our own page.
In the next lesson we will broadcast it to all users and some more interesting things.

