const socket = io('http://localhost:3000');
const messageForm = document.getElementById('send-container');
const messageInput = document.getElementById('message-input');
const messageContainer = document.getElementById('message-container');

messageForm.addEventListener('submit', onSubmit);

function onSubmit(event){
  event.preventDefault();
  const message = messageInput.value;
  socket.emit('send-message', message);
  messageInput.value = '';
  addMessage(`You: ${message}`);
};

function addMessage(message) {
  const messageElement = document.createElement('div')
  messageElement.innerText = message
  messageContainer.append(messageElement)
};