# Lesson 1
In this lesson, we will prepare the bases of the needed structure for the `Socket.IO` package.
## Step 1: Initialization of project
We assume that you have already installed `NodeJS` on your system if you don't please head [over here](https://nodejs.org/) and install it.
Now first let's initialize a node project by opening `terminal` for mac and Linux or `command prompt` for windows and type this:
```bash
npm init
```
it will start to ask you some questions to create your `package.json` file. you can just past them by hit `Enter` But I encourage you to put some valuable information there.
```BASH
> npm init
This utility will walk you through creating a package.json file.
It only covers the most common items and tries to guess sensible defaults.

See `npm help json` for definitive documentation on these fields
and exactly what they do.

Use `npm install <pkg>` afterwards to install a package and
save it as a dependency in the package.json file.

Press ^C at any time to quit.
package name: (----) chatroom
version: (1.0.0) 0.1.0
description: This is a project to create a chatroom
entry point: (index.js) server.js
test command: 
git repository: 
keywords: chat,socketio
author: Nima Azaraeen
license: (ISC) MIT
About to write to /Users/nimaaz/Sites/package.json:

{
  "name": "chatroom",
  "version": "0.1.0",
  "description": "This is a project to create a chatroom",
  "main": "server.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [
    "chat",
    "socketio"
  ],
  "author": "Nima Azaraeen",
  "license": "MIT"
}


Is this OK? (yes)
```
Good! now that we defined our project, let's get our dependencies. for this project, we will only use the package `Socket.IO` which has its own dependencies but `npm` will handle it on its own. So let's type this in the terminal:
```bash
npm install socket.io
```
## Step 2: Create a server
First, we will create a file called `server.js` and inside that, we will define our  first variable, which is a default `Http` server by `nodejs` :
```javascript
const server = require('http').createServer();
```
Next, we will create our `socket` using this server:
```javascript
const io = require('socket.io')(server);
```
Now we need to specify which kind of event we are waiting for in this server, let's take a look:
```javascript
io.on('connection', client => {
  client.on('event', data => { /* do something */ });
  client.on('disconnect', () => { /* do something */ });
});
```
In this code we are specifing that `on` the event of connection a function with the parameter of `client` must be executed. Then inside this connection event function we can create our own events (as you can see in line 2) and Also we can handle the disconnection event. For now we will just put two `console.log`s in the `connection` and `disconnect` to take a look at how it works. One last thing is to add a `port` for the server to listen to for incomming connection. so to this point now, the code in the `server.js` looks like this:
```javascript
const server = require('http').createServer();
const io = require('socket.io')(server);
io.on('connection', client => {
  console.log('someone connected');
  client.on('disconnect', () => { 
  	console.log('someone disconnected');
  });
});
server.listen(3000);
```
## Step 3: Client
Enough with the server. let create our clients who will connect to this server.
For now, we will connect locally to this server. So our server address is: `http://localhost:3000` or `http://127.0.0.1`. For the client, we will create one file called `script.js`. For now, we will only write one line of code in this file. connection to server:
```javascript
const socket = io('http://localhost:3000');
```
Then in order to use this connection, we will create one simple `HTML` code that loads `socket.io` client script and our `script.js` called `index.html`:
```html
<!doctype html>
<html>
  <head>
    <title>chat room</title>
    <script defer src="http://localhost:3000/socket.io/socket.io.js"></script>
    <script defer src="script.js"></script>
  </head>
  <body>
    <p> hello world </p>
  </body>
</html>
```
## Final step: testing the connection
This is not a step actually, but rather a view of what we have done so far.
1- let's run our server by running this command into our terminal:
```bash
node server
```
Now your server is waiting for socket connections.
2- Run the `index.html` by double-clicking on the file.
3- Look at the terminal and you will see the `server` script is showing a connection has been made by showing `someone connected` and if you close the window terminal will show `someone disconnected`
## Well done
Now we have a working server and a client that is connected to each other, in the next lesson we will start sending messages.