# Lesson 3
In this lesson we are going to make our chat room effective. 
## Step 1: add username capabilities
In order to have a chatroom, we need to have users' names, so let at the page load (AKA connection), get it with one normal javascript prompt in `script.js` and then `emit` it to the server with the event of `new-user`. After just as a reminder on where the user joined in the chat we will use our function of `addMessage()` to say "You joined":
```javascript
const name = prompt('What is your name?');
client.emit('new-user', name);
addMessage('You joined');
```
So now we need to go to `server.js` and handle this event and make use of it. First lets create an object that will hold our all users data:
```javascript
const users = {}
```
Now we can create another event handler on our server to listen this event:
```javascript
const users = {};

io.on('connection', client => {
  console.log('someone connected');
  socket.on('new-user', name => {
    console.log(`${name} connected`);
  })
  client.on('send-message', message => {
    console.log(`someone said ${message}`);
  });
  client.on('disconnect', () => { 
    console.log('someone disconnected');
  });
});
```
The intresting thing about `client` object that is an input the the whole function in this server, is a `socket` object and has some important and usfull properties. One of these object properties, is `id` and each connection to this `socket server` has one unique `id` that we can use as a `key` in our `user` object for each chatter that joins:
```javascript
...
io.on('connection', client => {
  console.log('someone connected');
  socket.on('new-user', name => {
    users[client.id] = name
  });
...
```
## Step 2: sending messages back to clients
Up untill now we were sending data to server now is servers turn to send messages to cliens. To do so we will use a method of the `socket` library called boardcast and as we want to send an event to the clients, we will use `emit`:
```javascript
...
client.on('new-user', name => {
  users[client.id] = name;
  client.broadcast.emit('user-connected', name);
});
...
```
and surly we need to we need to handle that in the clientside on our `script.js`
```javascript
client.on('user-connected', name => {
  addMessage(`${name} connected`)
});
```
## Step 3: adding some functionalities
Also we need to broadcast the message a client sent to server, to all clients, and the fact that one user was dissconnected, so in the server, we will get rid of all the console logs and `emit` some events to client:
```javascript
io.on('connection', client => {
  client.on('new-user', name => {
    users[client.id] = name
    client.broadcast.emit('user-connected', name)
  })
  client.on('send-chat-message', message => {
    client.broadcast.emit('chat-message', { message: message, name: users[client.id] })
  })
  client.on('disconnect', () => {
    client.broadcast.emit('user-disconnected', users[client.id])
    delete users[client.id]
  })
})
```